package com.hcl.springbootex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springbootex.entity.Fruits;
import com.hcl.springbootex.service.FruitsService;



@RestController
@RequestMapping("/v1/fruits")
public class FruitsController {
	
	@Autowired
	FruitsService service;
	
	@PostMapping(value = "/add",consumes = "application/json",produces = "application/json")
	public Fruits add(@RequestBody  Fruits fruit) {		
		Fruits added= service.addFruit(fruit);
		return added;
	}
	
	@DeleteMapping(value="/delete/{id}")
	public String delete(@PathVariable int id) {
		return service.deleteFruit(id);

	}
	
	

}
