package com.hcl.springbootex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springbootex.entity.Fruits;
import com.hcl.springbootex.repository.FruitsRepository;

@Service
public class FruitsServiceImpl implements FruitsService{

	@Autowired
	FruitsRepository repo;
	
	@Override
	public Fruits addFruit(Fruits fruit) {
		// TODO Auto-generated method stub
		return repo.save(fruit);
	}

	@Override
	public String deleteFruit(int id) {
		// TODO Auto-generated method stub
		repo.deleteById( id);
		return "deleted"+id;
	}
	

}
