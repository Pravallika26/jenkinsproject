package com.hcl.springbootex.service;

import com.hcl.springbootex.entity.Fruits;

public interface FruitsService {
	
	public Fruits addFruit(Fruits fruit);
	
	public String deleteFruit(int id);

}
