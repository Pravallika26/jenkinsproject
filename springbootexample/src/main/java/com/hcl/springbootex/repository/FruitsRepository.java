package com.hcl.springbootex.repository;

import org.springframework.data.repository.CrudRepository;

import com.hcl.springbootex.entity.Fruits;

public interface FruitsRepository extends CrudRepository<Fruits, Integer>{

}
