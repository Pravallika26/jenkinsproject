package com.hcl.jpql.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.hcl.jpql.entity.Customer;

public interface PagingRepository extends PagingAndSortingRepository<Customer, Integer>{

}
