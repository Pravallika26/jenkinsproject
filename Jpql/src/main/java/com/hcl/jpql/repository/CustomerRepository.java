package com.hcl.jpql.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hcl.jpql.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{
	@Query(
			value = "select * from customer c where c.city = :cityname",
			nativeQuery =true
			
			)
	public  List<Customer>  getCustomerByCity(@Param("cityname") String city);  //:city -named param

	//@Transactional we can also use here
	@Modifying
	@Query(
			
			value="update customer set city = ?1  where  name = ?2",
			nativeQuery = true
			)
		public  int   updateCustomerByCity(String city,String name);

	
//	@Query(
//			value="delete from customer c where c.city = :city_name",
//			nativeQuery = true
//			)
//	public String deleteCustomerByCity(String city);
//	
	
	
	
	
}
