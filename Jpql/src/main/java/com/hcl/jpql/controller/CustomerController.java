package com.hcl.jpql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.jpql.entity.Customer;
import com.hcl.jpql.service.CustomerService;

@RestController
@RequestMapping("/v1/Customer")
public class CustomerController {

	@Autowired
	CustomerService service;

	@PostMapping("/add")
	public Customer addCustomer(@RequestBody Customer customer) {
		return service.addCustomer(customer);

	}

	@GetMapping("/get/{id}")
	public Customer getCustomerById(@PathVariable int id) {
		return service.getCustomerById(id);
	}

	@GetMapping("/getbycity/{city}")
	public List<Customer> getCustomersByCity(@PathVariable String city) {
		return service.getCustomerByCity(city);
	}

	@PutMapping("/update")
	public ResponseEntity<String> updateCustomerByName(@RequestBody Customer customer) {

		ResponseEntity<String> response = null;

		int count = service.updateCustomerByCity(customer.getCity(), customer.getName());

		if (count > 0) {

			response = new ResponseEntity<String>("record updated", HttpStatus.OK);

		} else {
			response = new ResponseEntity<String>("update failed ", HttpStatus.BAD_REQUEST);
		}

		return response;
	}
//	
//	@DeleteMapping("/delete/{city}")
//	public String deleteCustomerByCity(@PathVariable String city) {
//		service.deleteCustomerByCity(city);
//		return "deleted";
//	
//	}	
//	
	@GetMapping("/getbypage")
	public List<Customer>  getCustomersByPage(){	
		return service.selectByPaging();	
	}
	
	
	
}