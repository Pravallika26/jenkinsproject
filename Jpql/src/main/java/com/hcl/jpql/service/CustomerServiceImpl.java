package com.hcl.jpql.service;

import java.awt.print.Pageable;
import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.hcl.jpql.entity.Customer;
import com.hcl.jpql.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository repo;

	@Override
	public Customer addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return repo.save(customer);
	}

	@Override
	public Customer getCustomerById(int id) {
		// TODO Auto-generated method stub
		return repo.findById(id).orElse(new Customer());
	}

	@Override
	public List<Customer> getCustomerByCity(String city) {
		// TODO Auto-generated method stub
		return repo.getCustomerByCity(city);
	}

	@Transactional
	@Override
	public int updateCustomerByCity(String city, String name) {
		// TODO Auto-generated method stub
		return repo.updateCustomerByCity(city, name);
	}

//	@Override
//	public String deleteCustomerByCity(String city) {
//		// TODO Auto-generated method stub
//		repo.deleteCustomerByCity(city);
//		return "deleted";
//	}

	@Override
	public List<Customer> selectByPaging() {
		// TODO Auto-generated method stub
		org.springframework.data.domain.Pageable firstPageWithThreeRecords = PageRequest.of(0, 3);
		org.springframework.data.domain.Pageable secondPageWithTwoRecords = PageRequest.of(1, 4);

		System.out.println(repo.findAll(firstPageWithThreeRecords).getContent());
		System.out.println(repo.findAll(firstPageWithThreeRecords).getTotalElements());
		System.out.println(repo.findAll(firstPageWithThreeRecords).getTotalPages());

		Stream<Customer> stream = repo.findAll(secondPageWithTwoRecords).stream();
		stream.forEach(System.out::println);
		//return repo.findAll(firstPageWithThreeRecords).getContent();
		return repo.findAll(secondPageWithTwoRecords).getContent();
	}
}
