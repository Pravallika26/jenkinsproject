package com.hcl.jpql.service;

import java.util.List;

import com.hcl.jpql.entity.Customer;

public interface CustomerService {

	public Customer addCustomer(Customer customer);

	public Customer   getCustomerById(int id);

	public List<Customer> getCustomerByCity(String city);

	public int updateCustomerByCity(String city, String name);

//	public String deleteCustomerByCity(String city);
	
	public List<Customer> selectByPaging();
	
	
}
